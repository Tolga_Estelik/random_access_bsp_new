package raf;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created: 16.01.2023
 *
 * @author Tolga Estelik {tolga}
 */
public class CheckInt {

    public static void createFile(String filename, double... values) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile(filename, "rw")) {
            if (raf.length() > 0 && isValidFile(filename)) {
                for (double value : values) {
                    append(filename, value);
                }
            } else if (raf.length() > 0 && !isValidFile(filename)) {
                raf.setLength(0);
                int control = values.length;
                raf.writeInt(control);
                for (double v : values) {
                    raf.writeDouble(v);
                }

            } else {
                int control = values.length;
                raf.writeInt(control);
                for (double v : values) {
                    raf.writeDouble(v);
                }
            }

        } catch (FileNotFoundException e) {
            System.err.println("Fehler beim öffnen der Datei!" + e);
        }
    }

    public static boolean isValidFile(String filename) throws IOException {
        boolean check = false;
        try (RandomAccessFile raf = new RandomAccessFile(filename, "r")) {
            int control = raf.readInt();
            for (int i = 0; i < control; i++) {
                try {
                    raf.readDouble();
                } catch (EOFException e) {
                    return false;
                }
            }
            try {
                raf.readByte();
            } catch (EOFException e) {
                return true;
            }

        } catch (FileNotFoundException e) {
            System.err.println("Fehler beim öffnen der Datei!" + e);
        }
        return check;
    }

    public static String getFileInfo(String filename) throws IOException {
        if (isValidFile(filename)) {
            StringBuilder stringBuilder = new StringBuilder();
            try (RandomAccessFile raf = new RandomAccessFile(filename, "r")) {
                int control = raf.readInt();
                stringBuilder.append(control);
                stringBuilder.append("\n");
                for (int i = 0; i < control; i++) {
                    String s = String.format("%.2f ", raf.readDouble());
                    stringBuilder.append(s);

                }
            } catch (FileNotFoundException e) {
                System.err.println("Fehler beim öffnen der Datei!" + e);
            }
            return stringBuilder.toString().trim();
        } else {
            return "invalid";
        }
    }


    public static void append(String filename, double toAppend) throws IOException {
        int control;
        if (isValidFile(filename)) {
            try (RandomAccessFile raf = new RandomAccessFile(filename, "rw")) {

                raf.seek(0);
                control = raf.readInt() + 1;
                raf.seek(0);
                raf.writeInt(control);
                raf.seek(raf.length());
                raf.writeDouble(toAppend);


            } catch (FileNotFoundException e) {
                System.err.println("Fehler beim öffnen der Datei!" + e);
            }
        } else {
            throw new IllegalArgumentException("Keine Gültige Datei!" + filename);
        }
    }
}
