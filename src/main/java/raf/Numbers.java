package raf;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

/**
 * Created: 07.02.2023
 *
 * @author Tolga Estelik {tolga}
 */
public class Numbers {

    public static void createFile(String fileName, List<Number> numberList) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile(fileName, "rw")) {
            for (Number n : numberList) {
                if (n instanceof Integer) {
                    raf.writeByte(0);
                    raf.writeInt((Integer) n);
                } else if (n instanceof Double) {
                    raf.writeByte(1);
                    raf.writeDouble( (Double) n);
                }
            }


        } catch (FileNotFoundException e) {
            System.err.println("Fehler beim öffnen der Datei!" + e);
        }

    }

    public static List<Number> getContents(String fileName) throws IOException {
        List<Number> numberArrayList = new ArrayList<>();
        try (RandomAccessFile raf = new RandomAccessFile(fileName, "r")){
            for (int i = 0; i < raf.length(); i++) {
                try {
                    byte b = raf.readByte();
                    if (b == 0) {
                        try{
                            numberArrayList.add(raf.readInt());
                        }catch (EOFException e){
                            throw new IllegalArgumentException();
                        }

                    } else if (b == 1) {
                        try{
                            numberArrayList.add(raf.readDouble());
                        }catch (EOFException e){
                            throw new IllegalArgumentException();
                        }
                    }
                }catch (EOFException e){
                    return numberArrayList;
                }
            }
        }
        return numberArrayList;
    }


    public static Map<String, Set<Number>> groupByType(List<? extends Number> numbers) {
        Map<String, Set<Number>> groupMap = new TreeMap<>();

        for (Number n: numbers) {
            Set<Number> set = new TreeSet<>();
            if (groupMap.containsKey(n.getClass().toString().split("\\.")[2])){
                groupMap.get(n.getClass().toString().split("\\.")[2]).add(n);
            }else{
                set.add(n);
                groupMap.put(n.getClass().toString().split("\\.")[2], set);

            }

        }
        return groupMap;
    }
}
